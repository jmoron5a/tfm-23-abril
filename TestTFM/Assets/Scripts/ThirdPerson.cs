﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPerson : MonoBehaviour
{
    // distancia de separacion entre la camara y el player
    public Vector3 offset;
    // transform del objetivo
    private Transform target;
    //velocidad con la que se mueve la camara
    [Range(0,1)]
    public float lerpValue;
    // sensibilidad con la que girara la camara

    public bool rotating = false;
    // Start is called before the first frame update
    void Start()
    {
        target = GameObject.Find("Player").transform;
    }
    private void FixedUpdate()
    {
        if (rotating == true) return;
        transform.position = Vector3.Lerp(transform.position, target.position + offset, lerpValue);
        transform.LookAt(target);   
    }

    public void Rotate()
    {
        if (Input.GetMouseButton(0))
        {
            rotating = false;
        }
    }
}
