﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLookCamera : MonoBehaviour
{
    public GameObject player;
    void LateUpdate()
    {
        Vector3 rot = Camera.main.transform.localEulerAngles;
        player.transform.eulerAngles = new Vector3(player.transform.eulerAngles.x, rot.y, player.transform.eulerAngles.z);
    }
}
