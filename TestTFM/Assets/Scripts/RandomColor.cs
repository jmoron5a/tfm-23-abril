﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
    #region VARIABLES --------------------------------------------
    public Color color;
    public Renderer[] renders;
#endregion

#region EVENTS --------------------------------------------
    // Start is called before the first frame update
    void Start()
    {
       renders = gameObject.GetComponentsInChildren<Renderer>();
       Randomize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
#endregion
#region METHODS --------------------------------------------
    [ContextMenu("Randomizador")]
    public void Randomize()
    {
        foreach (Renderer r in renders)
        {
            color = new Color(Random.Range(0.2f, 0.6f), Random.Range(0.2f, 0.6f), Random.Range(0.2f, 0.6f));
            r.material.color = color;
        }
    }
#endregion
}
